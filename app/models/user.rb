class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :comments
  has_many :articles
  
  # update, edit, destroy
  def owner?(resource)
    self.articles.include?(resource) || self.comments.include?(resource)
  end
  
  def can?(comment)
  self.comments.include?(comment) || self.articles.include?(comment.article)
  end
end
