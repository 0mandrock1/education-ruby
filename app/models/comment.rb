class Comment < ApplicationRecord
  belongs_to :article
  belongs_to :user
  
  enum status: [ :unapproved, :approved ]

  after_create :approved!, if: 'self.article.user == self.user'
end
