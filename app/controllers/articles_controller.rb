class ArticlesController < ApplicationController
  before_filter :authenticate_user!, :except => [:index]
  before_action :set_article, only: [:show, :edit, :update, :destroy]
  before_action :check_ability, only: [:edit, :update, :destroy]

    def index
        @articles = Article.all
    end

    def show
    end

    def new
      @article = Article.new
    end

    def edit
    end

    def create
        @article = Article.new(article_params)
        @article.user_id = current_user.id
        if @article.save
        redirect_to @article
        else
          render 'new'
        end
        
    end
    
    def update
      if @article.update(article_params)
        redirect_to @article
      else
        render 'edit'
      end
    end

    def destroy
      @article.destroy

      redirect_to articles_path
    end

    private
    def article_params
        params.require(:article).permit(:title, :text, :user_id)
    end
    
    def check_ability
      raise "you cant do it!" unless current_user.owner? @article
    end
    
    def set_article
      @article = Article.find(params['id'])
    end
end
