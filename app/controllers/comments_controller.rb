class CommentsController < ApplicationController
    before_action :check_destroy_ability, only: [:destroy]

    def create
        @article = Article.find(params['article_id'])
        @comment = @article.comments.new(comment_params)
        @comment.user = current_user
        if @article.save
          redirect_to @article
        else
          render 'new'
        end
    end

    def destroy
        @article = Article.find(params['article_id'])
        @comment = @article.comments.find(params['id'])
        @comment.destroy
        redirect_to article_path(@article)
    end
    
    def approve
        comment = Comment.find(params[:comment_id])
        raise "you cant do it!" unless comment.article.user == current_user
        comment.update(approved:true)
        redirect_to comment.article
    end

    private
    def comment_params
        params.require(:comment).permit(:body)
    end
    
    def check_destroy_ability
    end
end
