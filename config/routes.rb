Rails.application.routes.draw do
    
  devise_for :users
  get 'persons/profile', as: 'user_root'

    resources :articles do
      resources :comments do
        post 'approve', action: 'approve'
      end
    end
    


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'welcome#index'
end
